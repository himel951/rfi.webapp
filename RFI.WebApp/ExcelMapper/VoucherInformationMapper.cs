﻿using ExcelMapper;
using RFI.WebApp.ViewModel;

namespace RFI.WebApp.ExcelMapper
{
    public class DataType1ViewModelMapper : ExcelClassMap<DataType1ViewModel>
    {
        public DataType1ViewModelMapper()
        {
            Map(x => x.Name).WithColumnIndex(0);
            Map(x => x.BookingPeriod).WithColumnIndex(1);
            Map(x => x.ClosedDateFrom).WithColumnIndex(2);
            Map(x => x.ClosedDateTo).WithColumnIndex(3);
        }
    }

    public class DataType2ViewModelMapper : ExcelClassMap<DataType2ViewModel>
    {
        public DataType2ViewModelMapper()
        {
            Map(x => x.Name).WithColumnIndex(0);
            Map(x => x.BookingPeriod).WithColumnIndex(1);
            Map(x => x.ClosedDateFrom).WithColumnIndex(2);
            Map(x => x.ClosedDateTo).WithColumnIndex(3);
            Map(x => x.BookingID).WithColumnIndex(4);
        }
    }
}
