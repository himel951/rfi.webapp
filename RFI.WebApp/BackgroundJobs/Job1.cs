﻿using Hangfire;
using Hangfire.Annotations;
using Hangfire.Server;
using RFI.WebApp.Pages;
using RFI.WebApp.Services;

namespace RFI.WebApp.BackgroundJobs
{
    public class RFIJobs //: IBackgroundProcess
    {
        public IExcelVoucherReaderService ExcelVoucherReader { get; }
        private readonly ILogger<IndexModel> _logger;
        private readonly IBackgroundJobClient _backgroundJob;

        public RFIJobs(ILogger<IndexModel> logger, IExcelVoucherReaderService excelVoucherReader, IBackgroundJobClient backgroundJob)
        {
            ExcelVoucherReader = excelVoucherReader;
            _logger = logger;
            _backgroundJob = backgroundJob;
        }
        public void Initialize()
        {
            //_backgroundJob.Create()
            ExcelVoucherReader.GetAndSaveVoucherInformation();
        }
    }
}
