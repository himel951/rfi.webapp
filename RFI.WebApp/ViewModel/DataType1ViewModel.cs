﻿namespace RFI.WebApp.ViewModel;

public class DataType1ViewModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public DateTime BookingPeriod { get; set; }
    public DateTime ClosedDateFrom { get; set; }
    public DateTime ClosedDateTo { get; set; }
}