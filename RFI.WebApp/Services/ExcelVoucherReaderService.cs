﻿using Azure.Core;
using ExcelMapper;
using RFI.WebApp.ExcelMapper;
using RFI.WebApp.Models;
using RFI.WebApp.RFIContext;
using RFI.WebApp.ViewModel;
using System.ComponentModel.DataAnnotations;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace RFI.WebApp.Services;

public class ExcelVoucherReaderService : IExcelVoucherReaderService
{
    private readonly RFIDbContext _databaseContext;
    private readonly IWebHostEnvironment _hostingEnvironment;
    static string SheetName = "Sheet 1";
    public ExcelVoucherReaderService(RFIDbContext databaseContext, IWebHostEnvironment hostingEnvironment)
    {
        _databaseContext = databaseContext;
        _hostingEnvironment = hostingEnvironment;
    }

    public void GetAndSaveVoucherInformation()
    {


        // Get the path to the wwwroot folder
        string wwwRootPath = _hostingEnvironment.WebRootPath;

        // Specify the relative path to your file within the wwwroot folder
        string relativeFilePath = "file-storage";

        // Combine the paths to get the full path to the file
        string dirPath = Path.Combine(wwwRootPath, relativeFilePath);
        string destPathSuccess = Path.Combine(wwwRootPath, "Processed/success");
        string destPatherror = Path.Combine(wwwRootPath, "Processed/error");

        string[] allfiles = Directory.GetFiles(dirPath, "*.*", SearchOption.AllDirectories);

        foreach (string file in allfiles)
        {
            bool isProcessed = true;
            var filePath = Path.Combine(dirPath, file);
            var fileName = Path.GetFileName( file);
            try
            {


                if (fileName.StartsWith("d1"))
                {
                    ReadDataType1(filePath);

                }
                else if (fileName.StartsWith("d2"))
                {
                    ReadDataType2(filePath);

                }
                else
                {
                    isProcessed = false;
                }

            }
            catch (Exception e)
            {
                isProcessed = false;
            }
            if (isProcessed)
            {
                File.Copy(filePath, Path.Combine(destPathSuccess, fileName),true);
            }
            else
            {
                File.Copy(filePath, Path.Combine(destPatherror, fileName), true);
            }


        }
    }

    void ReadDataType1(string filePath)
    {

        using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
        {
            using (var importer = new ExcelImporter(stream))
            {
                importer.Configuration.SkipBlankLines = true;
                importer.Configuration.RegisterClassMap<DataType1ViewModelMapper>();
                ExcelSheet importSheet = importer.ReadSheet(SheetName);
                importSheet.HasHeading = true;

                var dataViewModel = importSheet.ReadRows<DataType1ViewModel>()
                    .ToList()
                    .Select(x => new DataType1ViewModel()
                    {
                        Id = Guid.NewGuid(),
                        Name = x.Name,
                        BookingPeriod = x.BookingPeriod,
                        ClosedDateFrom = x.ClosedDateFrom,
                        ClosedDateTo = x.ClosedDateTo,
                    }).ToList();

                if (dataViewModel != null && dataViewModel.Any())
                {

                    var data1 = dataViewModel.Select(x => new DataType1()
                    {
                        Id = Guid.NewGuid(),
                        Name = x.Name,
                        BookingPeriod = x.BookingPeriod,
                        ClosedDateFrom = x.ClosedDateFrom,
                        ClosedDateTo = x.ClosedDateTo
                    });

                    _databaseContext.DataType1.AddRange(data1);

                    int rowAffect = _databaseContext.SaveChanges();
                }

            }

        }



    }

    void ReadDataType2(string filePath)
    {
        using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
        {
            using (var importer = new ExcelImporter(stream))
            {
                importer.Configuration.SkipBlankLines = true;
                importer.Configuration.RegisterClassMap<DataType2ViewModelMapper>();
                ExcelSheet importSheet = importer.ReadSheet(SheetName);
                importSheet.HasHeading = true;

                var dataViewModel = importSheet.ReadRows<DataType2ViewModel>()
                    .ToList()
                    .Select(x => new DataType2ViewModel()
                    {
                        Id = Guid.NewGuid(),
                        Name = x.Name,
                        BookingPeriod = x.BookingPeriod,
                        ClosedDateFrom = x.ClosedDateFrom,
                        ClosedDateTo = x.ClosedDateTo,
                        BookingID = x.BookingID,
                    }).ToList();

                if (dataViewModel != null && dataViewModel.Any())
                {

                    var dataModel = dataViewModel.Select(x => new DataType2()
                    {
                        Id = Guid.NewGuid(),
                        Name = x.Name,
                        BookingPeriod = x.BookingPeriod,
                        ClosedDateFrom = x.ClosedDateFrom,
                        ClosedDateTo = x.ClosedDateTo,
                        BookingID = x.BookingID,
                    });

                    _databaseContext.DataType2.AddRange(dataModel);

                    int rowAffect = _databaseContext.SaveChanges();
                }
            }
        }

    }
}