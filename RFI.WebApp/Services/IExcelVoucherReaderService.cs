﻿using RFI.WebApp.ViewModel;

namespace RFI.WebApp.Services;

public interface IExcelVoucherReaderService
{
    void GetAndSaveVoucherInformation();
}