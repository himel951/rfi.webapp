﻿using Microsoft.EntityFrameworkCore;
using RFI.WebApp.Models;

namespace RFI.WebApp.RFIContext;

public class RFIDbContext : DbContext
{
    public RFIDbContext(DbContextOptions<RFIDbContext> options) : base(options)
    {

    }
    public DbSet<DataType1> DataType1 { get; set; }
    public DbSet<DataType2> DataType2{ get; set; } 
}
