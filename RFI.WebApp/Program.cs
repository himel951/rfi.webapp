using Hangfire;
using Hangfire.Common;
using Microsoft.EntityFrameworkCore;
using RFI.WebApp.BackgroundJobs;
using RFI.WebApp.RFIContext;
using RFI.WebApp.Services;

namespace RFI.WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();

            builder.Services.AddDbContext<RFIDbContext>(options => {
                var config = builder.Configuration;
                var connectionString = config.GetConnectionString("RFIDbContext");

                options.UseSqlServer(connectionString);
            });

            builder.Services.AddHangfire(x => x.UseSqlServerStorage(builder.Configuration.GetConnectionString("RFIDbContext")));
            builder.Services.AddHangfireServer();
            builder.Services.AddScoped<IExcelVoucherReaderService, ExcelVoucherReaderService>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }

            //app.UseHangfireDashboard("/dashbaord");
            app.UseHangfireDashboard();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapRazorPages();
            //Hangfire.BackgroundJob.Schedule<RFIJobs>("rfi-jobs",(j) => j.Initialize(), TimeSpan.FromHours(1));
            Hangfire.BackgroundJob.Schedule<RFIJobs>((j) => j.Initialize(), TimeSpan.FromHours(1));

            app.Run();

            //new RFIJobs()
            //var obj = Activator.CreateInstance(typeof(RFIJobs)) as RFIJobs;
            //obj.Initialize();
           

            //BackgroundJob.Schedule<RFIJobs>(() =>
            //{
            //    var obj = Activator.CreateInstance(typeof(RFIJobs)) as RFIJobs;
            //    obj.Initialize();
            //}, Cron.Yearly())
            //var manager = new RecurringJobManager();
            //manager.AddOrUpdate("some-id", new Job(() =>
            //{
            //    var obj = Activator.CreateInstance(typeof(RFIJobs)) as RFIJobs;
            //    obj.Initialize();
            //}), Cron.Yearly());

        }
    }
}