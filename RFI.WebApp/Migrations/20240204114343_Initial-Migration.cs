﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RFI.WebApp.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DataType1",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BookingPeriod = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ClosedDateFrom = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ClosedDateTo = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataType1", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataType2",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BookingPeriod = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ClosedDateFrom = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ClosedDateTo = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BookingID = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataType2", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataType1");

            migrationBuilder.DropTable(
                name: "DataType2");
        }
    }
}
