﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RFI.WebApp.RFIContext;

#nullable disable

namespace RFI.WebApp.Migrations
{
    [DbContext(typeof(RFIDbContext))]
    partial class RFIDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.15")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("RFI.WebApp.Models.DataType1", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("BookingPeriod")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ClosedDateFrom")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ClosedDateTo")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("DataType1");
                });

            modelBuilder.Entity("RFI.WebApp.Models.DataType2", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("BookingID")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("BookingPeriod")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ClosedDateFrom")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ClosedDateTo")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("DataType2");
                });
#pragma warning restore 612, 618
        }
    }
}
