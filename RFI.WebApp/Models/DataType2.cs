﻿namespace RFI.WebApp.Models
{
    public class DataType2
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime BookingPeriod { get; set; }
        public DateTime ClosedDateFrom { get; set; }
        public DateTime ClosedDateTo { get; set; }
        public string BookingID { get; set; }

    }
}
